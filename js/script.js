// let food = 'sinigang'; console.log(food);

// let num1 = 150; let num2 = 9; let sum = num1 + num2; console.log(sum);


// let num3 = '100'; let num4 = '90'; let product = num3 * num4;
// console.log(product);

// let isActive = true; console.log(isActive);


// let faveRestaurants = ['Jollibee', 'Mcdo', 'KFC', 'Mang Inasal',
// 'Chowking']; console.log(faveRestaurants);

// let faveArtist = { firstName: 'Bamboo', lastName: 'Manalac', stageName:
// 'Null', birthDay: 'March 21, 1975', age: '46', bestAlbum: 'As The Music
// Plays', bestSong: 'Noypi', isActive: 'true' }

// console.log(faveArtist);

// function divide (num1, num2) { return (num1 / num2); };

// let quotient = divide (200, 5); console.log("The result of the division is
// " + quotient);


// Mathematical operators

let num1 = 5; 
let num2 = 10; 
let num3 = 4; 
let num4 = 40;


// num1 = num1 + num4 num1 +- num4

// console.log(num1);

// num2 = num2 + num4;
num2 +=num4

console.log(num2);

// num1 = num1 * 2; num1 *= 2; console.log(num1);


let string1 = "Boston"; 
let string2 = "Celtics";

// string1 = string1 + string 2;

string1 += string2; 
console.log(string1);

// num1 = num1 - string1; num1 -= string1; console.log(num1);

let string3 = "Hello everyone"; 
let myArray = string3.split("",3);
console.log(myArray);

// Mathematical operation - follows MDAS

let mdasResult = 1 + 2 - 3 * 4 /5; 
console.log(mdasResult);

// PEMDAS - Parenthesis, exponents, multiplacation, division, addition and
// subtraction

let pemdasResult = 1 + (2-3) * (4/5); 
console.log(pemdasResult);

// Increment and Decrement Two types of Increment

let z = 1;

// pre-fix Incrementation ++z; console.log(z);

// post fix Incrementation

z++; console.log(z); 
console.log(z++); 
console.log(z);

// pre-fix vx post-fix

console.log(z++) 
console.log(z);

console.log(++z);

let n =1;

console.log(++n);  // ++n = 1 + n = 2;

console.log(n++); // n++ = n + 1 = 2 n is still equal to 2 n++ is do nothing
//as it show first the previous value it will on show the value of 3 if you
//type 
console.log(n)

// pre-fix and post fix decrementation console.log(z); console.log(z--);
console.log(z);

// comparison Operators // Equiality or Loose Eqaulity console.log(1 == 1);//
//true 
console.log('1' == 1);// true


// strict equality - use for passwords and encryption console.log(1 === 1);
console.log('1' === 1);

console.log('apple' == 'Apple') 
let isSame = 55 == 55 
console.log(isSame);

console.log(0 == false); // force coercion console.log(1 == true);
console.log(true == 'true');

console.log(true == '1'); 
console.log(false == '0');

// strict equality - checks both value and type console.log(1 === '1');
console.log('1' === '1'); 
console.log('Juan' === 'Juan'); 
console.log('Maria' === 'maria');


// Inequality Operators (!=) checks whether the operands are not equal and or
// have different value will do type coercion if the operands have different
// types;

console.log('1' !=1); // false > both operands are converted to numbers // '1' converted into number is 1 // 1 converted into number 1 // not inequal

console.log('James' !='Juan'); 
console.log(1 !="true"); // with type conversion: true was converted to 1 // "true" was converted into a number but results NaN // 1 is not equal to NaN // it is inequal

// strict inequalit operator (!==)> it checks whether the two operand have
// different values and will check if they have different types

console.log('5' !== 5);//true console.log( 5 !== 5); //false


let name1 ='Juan'; 
let name2 = 'Maria'; 
let name3 = 'Pedro'; 
let name4 =
'Perla';

let number1 = 50; 
let number2 = 60; 
let numString1 = "50"; 
let numString2 = "60"; 
console.log(numString1 == number1); 
console.log(numString1 === number1);
console.log(numString1 != number1); 
console.log(name4 !== name3);
console.log(name1 === 'Juan');

// Relational Comparison Operators

	// A comparison operator - check the relationship between the operands

let x = 500; let y = 700; let w = 8000; let numString3 = '5500'

// Greater than (>)

console.log( x > y ); console.log( w > y );

// less than (<) console.log(w < y); console.log(y >= y); // true
console.log(x < 1000); 
console.log(numString3 < 1000);//false
console.log(numString3 < 6000);//true 
console.log(numString3 < "Juan");// true - erratic or logical error

let isAdmin = false; 
let isRegistered = true; 
let isLegalAge = true;


// Operands

// T && T =T T && F =F F && T = G F && F = F



let authorization1 = isAdmin && isRegistered;
console.log(authorization1);//false

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2);//true

let authorization3 = isAdmin && isLegalAge; console.log(authorization3);
//false

let requiredLevel = 95; let requiredAge = 18;

let authorization4 = isRegistered && requiredLevel === 25;
console.log(authorization4); //false

let authorization5 = isRegistered && isLegalAge && requiredLevel === 95;
console.log(authorization5);


let userName1 ='gamer2021'; 
let userName2 = 'shadowMaster'; 
let userAge1 = 15;
let userAge2 = 30;


let registration1 = userName1.length > 9 && userAge1 >= requiredAge;
console.log(registration1); //false

let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2);

let registration3 = userName1.length > 8 && userAge2 >= requiredAge;


// OR Operator (|| - double pipe) // - or operator returns truie if a t least one of the operands are true 
// T || T = T // T || F = T // F || T = T // F || F = F


let userLevel = 100; let userLevel2 = 65;

let guildRequirement = isRegistered && userLevel >= requiredLevel && userAge1
>= requiredAge; console.log(guildRequirement); //false

guildRequirement = isRegistered || userLevel >= requiredLevel || userAge1 >=
requiredAge; console.log(guildRequirement); // true since there is one true

let guildRequirement2 = userLevel >= requiredLevel || userAge1 >= requiredAge;
console.log(guildRequirement2); //true since there is one true

let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin); // isAdmin value is false (line 217)  || 65 >= 95 (false) = F + F = False


//Not operator (!) it turns a boolean into the opposite value: T = F   F = T

let guildAdmin1 = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin1); 

//isAdmin is originally false but converted to true so the reuslt will be T +
//F = T


let opposite1 = !isAdmin; let opposite2 = !isLegalAge;

console.log(opposite1);// true - isAdmin original value = false
console.log(opposite2);// false - isLegalAge original value = true

//if - if statement will run a code block if the condition specified is true
//or results to true


const candy = 100; if (candy >= 100){ console.log('You got a cavity!') }


// if(true) { //	block of code //   };


let userName3 = "crusader_1993"; let userLevel3 = 25; let userAge3 = 30;

if(userName3.length > 10){ console.log("Welcome to the Game online!") }

//else statement will be run if the condition given is false of reults to
//false

if(userName3 >=10 && userLevel3 <= 25 && userAge3 >= requiredAge){
console.log("Thank you for joining Noobies Guild"); 
}
else { 
console.log("You too strong to be noob.");
}


// else if - else if executes a statement, if the previous or the original
// condition is false or resulted to false but another specified condition
// resulted to true.

if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
	console.log("Thank you for joining the noobies guild.");
} else if(userLevel3 > 25){
	console.log("You too strong to be noob.");
} else if(userAge3 < requiredAge){
	console.log("You are too young to join the guild.");
} else {
	console.log("End of the condition.");
}


// if-else in function
function addNum(num1, num2){
	if(typeof num1 === "number" && typeof num2 === "number"){
		console.log("Run only if both arguments passed are number types.");
		console.log(num1 + num2);
	} else {
		console.log("One or both of the argumetns are not numbers.")
	};
};

addNum(5, 2)

	// let customerName = prompt("Enter your name:");
	// if (customerName != null){
	// 	document.getElementbyID("welcome").innerHTML = "Hello" customerName;
	// }

	function login(username, password){
		if(typeof username === "string" && typeof password === "string"){
			console.log("both arguments are string.");
		}else if(password.length <= 8 && username.length <= 8){
			console.log("Credentials too short.");
		}if (password.length <= 8){
			console.log("password too short.");
		}if (username.length <= 8){
			console.log("Username is too short.");
		}
	}

	login('aaron_12345', '12345678')